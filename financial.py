#!/usr/bin/python

from __future__ import division, print_function
from collections import namedtuple
import scipy
import scipy.linalg
import pylab
import sys
import numpy as np


# Fit a line y = ax + b to a signal.
# Return a, b, and the residuals of (y - x)
def leastsq(signal, offset):
    X = scipy.array([[i + offset, 1] for i in xrange(len(signal))])
    a, b = scipy.linalg.lstsq(X, signal)[0]
    residuals = [(a*(t+offset) + b) - x for t, x in enumerate(signal)]
    SSR = np.sum([x**2 for x in residuals])
    print(SSR)
    return a, b, SSR


RegressionLine = namedtuple('RegressionLine', ['a', 'b', 'interval'])


def segmented_leastsq(signal, c):
    optimalRegressions = []
    lastBreakpoint = 0
    oldSSR = 0
    
    for currentIndex, datapoint in enumerate(signal[1:], 1):
        #Calculate the error of a new regression line that includes the next point
        #print("Running regression between %d and %d" % (lastBreakpoint, currentIndex + 1))
        a, b, SSR = leastsq(signal[lastBreakpoint:currentIndex + 1], offset=lastBreakpoint)

        marginalCost = SSR - oldSSR

        #compare this to the cost of adding a new segment to the segmented regression
        if marginalCost > c:
            # Finalize the current regression
            # Add it to the list of optimal regressions

            #print("adding %f %f" % (a, b))
            r = RegressionLine(a, b, (lastBreakpoint, currentIndex))
            optimalRegressions.append(r)
            lastBreakpoint = currentIndex
            oldSSR = 0

    
    #Add the very last regression line
    #optimalRegressions.append(RegressionLine(currentRegression.a, currentRegression.b, (lastBreakpoint, currentIndex)))

    return optimalRegressions  

# Fits a line y = ax + b to a signal
# Finds the minimum a, b of the objective
# sum_i w[i] * (signal[i] - (a*i + b))**2
def weighted_leastsq(signal, weights):
    w = scipy.sqrt(weights)
    X = scipy.array([[w[i]*i, w[i]] for i in xrange(len(signal))])
    a, b = scipy.linalg.lstsq(X, w*signal)[0]
    return a, b


def iterated_reweighted_least_sq(signal, weights, s):

    previous_a = None
    previous_b = None

    w = scipy.sqrt(weights)
    i = 0
    while 1:

        a, b = weighted_leastsq(signal, w)

        residuals = [(a*t + b) - x for t, x in enumerate(signal)]

        w = np.array([0 if residuals[i] > s else (1-(residuals[i]/s)**2)**2 for i in xrange(len(residuals))])

        #w = np.array([min(residuals[i]**2, s**2) for i in xrange(len(residuals))])

        # TODO figure out proper termination condition
        # For now, terminate when the coefficients never change
        if previous_a is not None and previous_b is not None and np.allclose(a, previous_a) and np.allclose(b, previous_b):
            break
        previous_a = a
        previous_b = b
        i +=1 

    return a, b, residuals
      


def signalFromFile(filename):
    with open(filename) as f:
        return [float(line) for line in f]
   


def graph(signal):
    pylab.ion()
    pylab.figure()
    pylab.plot(signal, 'g')
    pylab.ioff()



def findPeaks(signal):
    window = 21



# Computes the Short-Time Fourier Transform (STFT) of a signal, with a given
# window length, and shift between adjacent windows
def stft(x, window_len=4096, window_shift=2048):
    w = scipy.hamming(window_len)
    X = scipy.array([scipy.fft(w*x[i:i+window_len])
        for i in range(0, len(x)-window_len, window_shift)])
    return scipy.absolute(X[:,0:window_len/2])



def signalToCoordinates(signal):
    for i, x in enumerate(signal):
        yield (i, x)


def seizureFilter(seizure, filterBegin=2599, filterEnd=2694):

    #seizure = list(signalToCoordinates(seizure))
    matchedFilter = list(seizure)[filterBegin:filterEnd]


    #Graph the single pulse that we are using for our filter
    pylab.ion()
    pylab.figure()
    pylab.plot(matchedFilter)
    pylab.ioff()
    
    #Graph the convolution
    graphConvolution(seizure, matchedFilter)

def graphConvolution(signal, matchedFilter):
    convolution = convolve(signal, matchedFilter)
    pylab.ion()
    pylab.figure()
    pylab.plot(signal)
    pylab.plot(convolution)
    pylab.ioff()
    


def convolve(signal, stencil):
    #Assume the stencil is smaller than the signal
    result = np.zeros(len(signal) - len(stencil) + 1)
    stencil = stencil/np.sum(stencil)

    for i in xrange(len(result)):
        currentSlice = signal[i:i + len(stencil)]

        result[i] = np.dot(currentSlice, stencil)
        

    return result



def findPeaks(signal):
    stencil = np.array([0, 1, 0])
    return convolve(signal, stencil)



# Find the points at which the signal changes from negative to positive
def findCrossings(signal):
    result = []
    for i in xrange(1, len(signal)):
        if signal[i-1] < 0 and signal[i] >= 0:
            result.append(i)

    return result





if __name__ == '__main__':

    EUR_JPY = signalFromFile('EUR-JPY.data')
    regressions = segmented_leastsq(EUR_JPY, 200)

    print(len(regressions))

    pylab.ion()
    pylab.figure()
    pylab.plot(EUR_JPY, 'g')
    for r in regressions:
        pylab.axvline(r.interval[0], 0, np.max(EUR_JPY))
        x_axis = scipy.array(xrange(len(EUR_JPY)))
        pylab.plot(r.a*x_axis + r.b, 'r')
    #pylab.savefig('EUR-JPY.png')
    pylab.ioff()


    """


    #Read seizure data
    normal1 = signalFromFile('normal1.data')
    normal2 = signalFromFile('normal2.data')
    normal3 = signalFromFile('normal3.data')
    seizure1 = signalFromFile('seizure1.data')
    seizure2 = signalFromFile('seizure2.data')
    seizure3 = signalFromFile('seizure3.data')


    matchedFilter = list(seizure1)[2599:2694]

    convolution = convolve(seizure1, matchedFilter)
    peaks = findPeaks(findPeaks(convolution))


    #graphConvolution(normal1, matchedFilter)

    """


    """
    pylab.ion()
    pylab.figure()
    pylab.plot(seizure1)
    pylab.axvline(2599, 0, np.max(EUR_JPY), color='red')
    pylab.axvline(2694, 0, np.max(EUR_JPY), color='red')
    #pylab.plot(seizure2)
    #pylab.plot(seizure3)
    pylab.ioff()
    """



    """
    f = open('EUR-JPY.data')

    signal = [float(line) for line in f]
    weights = [1.0 for i in xrange(len(signal))]

    #a, b = weighted_leastsq(signal, weights)
    a, b, residuals = iterated_reweighted_least_sq(signal, weights, 4)


    x_axis = scipy.array(xrange(len(signal)))

    pylab.ion()
    pylab.figure()
    pylab.plot(signal, 'g')
    pylab.plot(a*x_axis + b, 'r')
    #pylab.savefig('EUR-JPY.png')
    pylab.ioff()

    raw_input('Press [enter] to continue')
    """

